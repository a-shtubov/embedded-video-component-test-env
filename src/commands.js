export default [
  {
    name: 'init',
    struct: `
    {
        type: 'init',
        mode: 'live',
        origin: 'origin',
        archive: {
          time: new Date(),
          storage: 'storage',
        },
        options: { archivePane: true },
    }`,
  },
  {
    name: 'reInit',
    struct: `
    {
      type: 'reInit',
      mode: 'live',
      origin: 'origin',
      archive: {
        time: new Date(),
        storage: 'storage',
      },
    }`,
  },
  {
    name: 'archive -> live',
    struct: `
    {
      type: 'live',
    }
    `,
  },
  {
    name: 'live -> archive',
    struct: `
    {
      type: 'archive',
    }
    `,
  },
  {
    name: 'set arch time',
    struct: `
    {
      type: 'setTime',
      time: new Date(),
    }
    `,
  },
  {
    name: 'play',
    struct: `
    {
      type: 'play',
    }
    `,
  },
  {
    name: 'stop',
    struct: `
    {
      type: 'stop',
    }
    `,
  },
];
