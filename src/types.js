export default `type AdditionalOpts = {
  archivePane?: boolean,
  forceMjpeg?: boolean,
};

type ArchiveOpts = {
  time: Date,
  storage: string,
};

type ComponentMode = 'live' | 'archive';

type InitCmd = {
  type?: 'init',
  mode?: ComponentMode,
  origin: string,
  archive: ArchiveOpts,
  options?: AdditionalOpts,
};

type SetTimeCmd = {
  type: 'setTime',
  time: Date,
};

type ReInitCmd = {
  type: 'reInit',
  mode?: ComponentMode,
  origin: string,
  archive: ArchiveOpts,
};

type ArchiveModeCmd = {
  type: 'archive',
};

type LiveModeCmd = {
  type: 'live',
};

type PlayCmd = {
  type: 'play',
};

type StopCmd = {
  type: 'stop',
};
`;
